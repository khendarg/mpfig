'''
Database-handling module

Use this for input/output
'''
from __future__ import division, print_function

import os
import re
import opm
LEGACY_URLLIB = False 
try: 
    import urllib.parse
    import urllib.request
except ImportError: 
    import urllib
    LEGACY_URLLIB = True
import time
import json

__all__ = opm.__all__[:]
__all__ += ['OpmDatabase']

class OpmDatabase(object):
    '''Container class for interacting with OPM data'''

    def __init__(self, prefix):
        '''Constructor

        prefix: directory containing all OPM stuff'''
        self.prefix = prefix

        self.indices = {}

    def fetch_pdb(self, identifier):
        '''Fetches general OPM data for given an iterable of PDB IDs. Use 'all' for all PDBs'''

        if identifier == 'all': keywordlist = ['']
        else: keywordlist = identifier[:]
        #start = ['']
        #delim = '\n\t ,'
        #if identifier == 'all': pass
        #else:
        #    if type(identifier) is str:
        #        for c in delim: 
        #            if c in identifier: 
        #                rawkeywords = identifier.split(c)
        #                start = []
        #                for pdbid in rawkeywords:
        #                    if not re.match('^[0-9][a-z]', pdbid):
        #                        raise ValueError('Invalid pdbid: {}'.format(pdbid))
        #                    else: pdbid = pdbid[:4].lower() + pdbid[4:]
        #                    start.append(pdbid)
        #        print(

        remoteprefix = 'https://lomize-group-opm.herokuapp.com/primary_structures'


        objects = []
        outf = open('{}/systems.tsv'.format(self.prefix), 'w')

        for kw in keywordlist:
            pagenum = 1
            lastpage = False

            while not lastpage:

                postdata = {}
                postdata['search'] = kw[:4]
                postdata['pageNum'] = str(pagenum)
                try: url = urllib.request.urlopen('{}?{}'.format(
                    remoteprefix, 
                    data=urllib.parse.urlencode(postdata)
                ))
                except AttributeError: 
                    url = urllib.urlopen('{}?search={}&pageNum={}'.format(
                        remoteprefix,
                        kw[:4],
                        pagenum)
                    )
                page = json.loads(url.read())
                if page['page_end'] >= page['total_objects']: lastpage = True
                else: print(page['page_end'], page['total_objects'], pagenum)

                pagenum += 1
                objects += page['objects']

                for struc in objects: 
                    outf.write('{}\t{}\n'.format(struc['pdbid'], json.dumps(struc)))
                objects = []
                outf.flush()

        outf.close()

    def fetch_subunits(self, identifierlist, buffersize=100):
        '''Retrieve subunits from OPM. Takes roughly 1 hour

        Can optionally retrieve a subset of OPM'''
        objbuffer = []
        outf = open('{}/subunits.tsv'.format(self.prefix), 'w')

        remoteprefix = 'https://lomize-group-opm.herokuapp.com/primary_structures'

        for index in identifierlist:
            try: url = urllib.request.urlopen('{}/{}'.format(remoteprefix, index))
            except AttributeError: url = urllib.urlopen('{}/{}'.format(remoteprefix, index))
            objbuffer.append(json.loads(url.read()))

            if len(objbuffer) >= buffersize:
                while objbuffer:
                    obj = objbuffer.pop(0)
                    outf.write('{}\t{}\n'.format(obj['pdbid'], json.dumps(obj)))
            else: time.sleep(0.1)
        while objbuffer:
            obj = objbuffer.pop(0)
            outf.write('{}\t{}\n'.format(obj['pdbid'], json.dumps(obj)))
            
        outf.close()

    def fetch(self, identifier, force=False):
        '''Fetch indices from OPM

        Runs both fetch_pdb and fetch_subunit'''
        fetchsys = True
        if not force and os.path.isfile('{}/systems.tsv'.format(self.prefix)): fetchsys = False
        if fetchsys: self.fetch_pdb(identifier)


        fetchsub = True
        if not force and os.path.isfile('{}/subunits.tsv'.format(self.prefix)): fetchsub = False
        if fetchsub: 
            idlist = []
            with open('{}/systems.tsv'.format(self.prefix)) as f:
                for l in f: 
                    if not l.strip(): continue
                    elif l.startswith('#'): continue
                    sl = l.split('\t')
                    if len(sl) < 2: raise ValueError('Invalid line: {}'.format(l))
                    obj = json.loads(sl[1])
                    idlist.append(obj['id'])
            self.fetch_subunits(idlist)

    def load(self, identifier='all', splitchains=False):
        '''Load indices from subunits.tsv'''
        primaries = set()
        secondaries = set()
        
        if identifier == 'all': getme = ['']
        else: getme = identifier

        outdata = {}
        with open('{}/subunits.tsv'.format(self.prefix)) as f:
            for n, l in enumerate(f):
                if not l.strip(): continue
                elif l.startswith('#'): continue
                sl = l.split('\t')
                if len(sl) < 2: raise ValueError('Invalid line: {}'.format(l))
                for start in getme: 
                    if sl[0].startswith(start):
                        obj = json.loads(sl[1])

                        primaries.add(obj['pdbid'])
                        for sec in obj['secondary_representations']: secondaries.add(sec['pdbid'])

            for pri in primaries: 
                try: secondaries.remove(pri)
                except KeyError: pass


            f.seek(0)
            for n, l in enumerate(f):
                if not l.strip(): continue
                elif l.startswith('#'): continue
                sl = l.split('\t')
                if len(sl) < 2: raise ValueError('Invalid line: {}'.format(l))

                if (sl[0] not in primaries) and (sl[0] not in secondaries): continue

                obj = json.loads(sl[1])

                for subunit in obj['subunits']:
                    if splitchains:
                        if sl[0][:4] not in outdata: outdata[sl[0][:4]] = {}
                        outdata[sl[0][:4]][subunit['protein_letter']] = self.parse_segment(subunit['segment'])
                    else:
                        outdata['{}_{}'.format(
                            sl[0], 
                            subunit['protein_letter']
                        )] = self.parse_segment(subunit['segment'])

                    for sec in obj['secondary_representations']:
                        if sec['pdbid'] in secondaries:
                            if splitchains:
                                if sec['pdbid'] not in outdata:
                                    outdata[sec['pdbid']] = {}
                                outdata[sec['pdbid']][subunit['protein_letter']] = self.parse_segment(subunit['segment'])
                            else:
                                outdata['{}_{}'.format(
                                    sec['pdbid'], 
                                    subunit['protein_letter']
                                )] = self.parse_segment(subunit['segment'])
        self.indices = outdata
        return outdata

    def save_assignments(self, identifier):
        '''Save assignments to ASSIGNMENTS.TSV for Deuterocol'''
        saveme = [''] if identifier == 'all' else identifier

        tmspreds = self.load(identifier)

        outf = open('{}/ASSIGNMENTS.TSV'.format(self.prefix), 'w')
        for k in sorted(tmspreds):
            spanstr = ''
            for span in tmspreds[k]: spanstr += '{}-{},'.format(*span)
            if spanstr: spanstr = spanstr[:-1]
            outf.write('{}\t{}\n'.format(k, spanstr))
        outf.close()

    def parse_span(self, s):
        '''Helper function for parsing away inconsistencies in TMS specification 
        strings'''
        s = re.sub(r',', ') 1(', s)
        s = re.sub(r'\n[A-Za-z]+', '', s)
        s = re.sub(r'\r?\n', '', s)
        s = re.sub(r'^\s*[0-9]+\(\s*', '', s)
        if '-' not in s:
            #5a63_C
            s = re.sub(r'([0-9]+)\s+([0-9]+)', r'\1-\2', s)
            start, end = s.split('-')
            while int(start) > int(end):
                start = start[1:]
            s = '{}-{}'.format(start, end)
        s = re.sub(r'\s+', '', s)
        s = re.sub(r'[0-9]+\(', '', s)
        s = re.sub(r'\)[^\-]*', '', s)
        s = re.sub(r'.*:', '', s)
        s = re.sub(r'^.*\(', '', s)
        return s


    def parse_segment(self, segment):
        '''Helper function for parsing away inconsistencies in subunit TMS 
        specification strings'''
        spanlist = re.split(r'\),?\s*[0-9][0-9]?\s*\(?', segment)
        #segstr = ''
        spans = []
        for span in spanlist:
            s = self.parse_span(span)
            spans.append([int(x) for x in s.split('-')])
            #if s.strip(): segstr += s + ','
        return spans

