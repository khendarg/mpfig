'''
A light Python API for OPM data
'''
from __future__ import division, absolute_import, print_function

__all__ = []
__all__.extend(['db'])

def in_spans(i, spanlist):
    for span in spanlist:
        if span[0] <= i <= span[1]: return True
    return False

def get_spanid(i, spanlist):
	for n, span in enumerate(spanlist):
		if span[0] <= i <= span[1]: return n
	return None
