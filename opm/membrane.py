'''
Membrane-related stuff

Use this for generating figures
'''
from __future__ import unicode_literals, division, print_function

import numpy as np
import Bio.PDB
import opm.db

import warnings
warnings.filterwarnings('ignore', 'PDBConstruction.*')
warnings.filterwarnings('ignore', '.*discontinuous.*')
warnings.filterwarnings('ignore', 'Used element')

try: CODE = Bio.PDB.protein_letters_3to1
except AttributeError: CODE = Bio.PDB.to_one_letter_code

class Pdb(object):
    def __init__(self, fn):
        self.fn = fn
        #TODO: add support for CIF
        parser = Bio.PDB.PDBParser()
        self.structure = parser.get_structure(fn, fn)

    def get_normal(self):
        coords = {}
        with open(self.fn) as f:
            for l in f:
                if not l.startswith('HETATM'): continue
                else: 
                    #print(l[13:14], l[31:])
                    if l[17:20] != 'DUM': continue
                    
                    #coords[l[13]].append(pos)
                    x = float(l[30:38])
                    y = float(l[38:46])
                    z = float(l[46:54])
                    pos = np.array([x, y, z])
                    #print(l)
                    #print('0123456789'*8)
                    try: coords[l[13]] = np.vstack([coords[l[13]], pos])
                    except KeyError: coords[l[13]] = np.array([pos])
        #print(np.mean(coords['O'], 0) - np.mean(coords['N'], 0))
        #for face in coords: print(face, np.mean(coords[face], 0)8)

    def simplify_tms(self, spans, subhels=4):
        backbone = {}

        import matplotlib
        matplotlib.use('TkAgg')
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        for model in self.structure:
            for chain in model:
                if chain.id not in spans: continue
                backbone[chain.id] = []
                for span in spans[chain.id]: backbone[chain.id].append([])

                for residue in chain:
                    if residue.get_resname() not in CODE: continue
                    if opm.in_spans(residue.id[1], spans[chain.id]):
                        tmsi = opm.get_spanid(residue.id[1], spans[chain.id])
                        for atom in residue:
                            if atom.id in ('N', 'CA', 'C'):
                                backbone[chain.id][tmsi].append(atom.get_coord())
                for tmsi in range(len(spans[chain.id])):
                    backbone[chain.id][tmsi] = np.array(backbone[chain.id][tmsi])
                    covars = np.cov(backbone[chain.id][tmsi].T)
                    val, vec = np.linalg.eig(covars)
                    bestval = 0
                    for i, x in enumerate(val): 
                        bestval = i if x == max(val) else bestval
                    secondval = 0
                    for i, x in enumerate(val):
                        secondval = i if x == sorted(val)[1] else secondval
                    thirdval = 0
                    for i, x in enumerate(val): 
                        thirdval = i if x == min(val) else thirdval 

                    atoms = backbone[chain.id][tmsi]
                    ax.plot(atoms[:,0], atoms[:,1], atoms[:,2])
                    centroid = np.mean(backbone[chain.id][tmsi], axis=0)
                    #ax.plot([centroid[0]], [centroid[1]], [centroid[2]], marker='+')

                    l = (val/np.std(atoms, axis=0))[bestval]
                    norms = np.vstack([[centroid], [l*vec[:,bestval].T + centroid]])
                    ax.plot(norms[:,0], norms[:,1], norms[:,2], color='r')

                    l = (val/np.std(atoms, axis=0))[secondval]
                    norms = np.vstack([[centroid], [l*vec[:,secondval].T + centroid]])
                    ax.plot(norms[:,0], norms[:,1], norms[:,2], color='g')

                    l = (val/np.std(atoms, axis=0))[thirdval]
                    norms = np.vstack([[centroid], [l*vec[:,thirdval].T + centroid]])
                    ax.plot(norms[:,0], norms[:,1], norms[:,2], color='b')
                    #print(bestval)
                    #print(val)
                    #print(vec)
            plt.show()

    def crossprod_tms(self, spans, subhels=2):
        crossprods = {}
        calphas = {}
        centroids = {}
        for model in self.structure:
            for chain in model:
                if chain.id not in spans: continue
                crossprods[chain.id] = {}
                centroids[chain.id] = {}
                calphas[chain.id] = []
                for i in range(len(spans[chain.id])): 
                    crossprods[chain.id][i] = []
                    centroids[chain.id][i] = []

                for residue in chain:
                    coords = []
                    if residue.get_resname() not in CODE: continue
                    if opm.in_spans(residue.id[1], spans[chain.id]):
                        for atom in residue:
                            if atom.id in ('N', 'CA', 'C'):
                                coords.append(atom.get_coord())
                            if atom.id == 'CA':
                                calphas[chain.id].append(atom.get_coord())

                            if atom.id == 'CA':
                                centroids[chain.id][opm.get_spanid(residue.id[1], spans[chain.id])].append(atom.get_coord())

                        if len(coords) < 3: continue
                        prod = np.cross(coords[1] - coords[0], coords[2] - coords[1])
                        crossprods[chain.id][opm.get_spanid(residue.id[1], spans[chain.id])].append(prod)
                
                for i in range(len(spans[chain.id])): 
                    crossprods[chain.id][i] = np.array(crossprods[chain.id][i])
                calphas[chain.id] = np.array(calphas[chain.id])
                for i in range(len(centroids[chain.id])):
                    centroids[chain.id][i] = np.array(centroids[chain.id][i])
                    #centroids[chain.id]

        import matplotlib
        matplotlib.use('TkAgg')
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        for chain in sorted(spans):
            ax.plot(calphas[chain][:,0], calphas[chain][:,1], calphas[chain][:,2], linewidth=0.5)

            for tmsi in crossprods[chain]:
                bounds = []
                tmlen = len(crossprods[chain][tmsi])
                for i in np.arange(0, tmlen, 1.*tmlen/subhels):
                    bounds.append(int(i))
                bounds.append(None)

                for boundi in range(len(bounds)-1):
                    start, end = bounds[boundi:boundi+2]
                    vec = np.mean(crossprods[chain][tmsi][start:end], axis=0)

                    if np.linalg.norm(vec) < 0.8: continue

                    nstart = centroids[chain][tmsi][0]
                    nend = nstart + vec
                    plotme = np.vstack([nstart, nend])

                    ax.plot(plotme[:,0], plotme[:,1], plotme[:,2], color='red')
                    ax.plot(plotme[1:,0], plotme[1:,1], plotme[1:,2], color='red', marker='.')

                    nstart = centroids[chain][tmsi][start]
                    nend = nstart + vec
                    plotme = np.vstack([nstart, nend])

                    ax.plot(plotme[:,0], plotme[:,1], plotme[:,2], color='red')
                    ax.plot(plotme[1:,0], plotme[1:,1], plotme[1:,2], color='red', marker='.')

                    ax.plot([0,vec[0]], [0,vec[1]], [0, np.abs(vec[2])], color='red')
                    ax.plot([vec[0]], [vec[1]], [np.abs(vec[2])], color='red', marker='.')
        plt.title(self.fn)
        plt.show()
                    

    def fit_by_pca(self, spans):
        #print(spans)
        calphas = np.array([])
        centroids = {}
        for model in self.structure:
            for chain in model:
                if chain.id not in spans: continue
                centroids[chain.id] = np.zeros(3)
                tmres = 0
                for residue in chain:
                    if residue.get_resname() not in CODE: continue
                    if opm.in_spans(residue.id[1], spans[chain.id]):
                        for atom in residue: 
                            #if atom.id in ('N', 'CA', 'C', 'O', 'CB'):
                            if atom.id in ('CA'):
                                tmres += 1
                                if calphas.any(): calphas = np.vstack([calphas, atom.get_coord()])
                                else: calphas = np.array([atom.get_coord()])
                                centroids[chain.id] += atom.get_coord()

                centroids[chain.id] /= tmres 
        std = np.std(calphas, axis=0)
        covars = np.cov(calphas[:,0], calphas[:,1])
        val, vec = np.linalg.eig(covars)
        

        #TODO: rewrite for the more general n-dimensional case (low-priority)

        pca = 0 if val[0] == max(val) else 1
        #print(i, val)

        tfed = np.array([])
        itfed = np.array([])
        iitfed = np.array([])
        for point in calphas:
            #vertxy = np.reshape(point[:2], (2, 1))
            #prod = np.dot(vec, vertxy)
            prod = np.dot(vec, point[:2])
            #squish away the other axes
            for i, c in enumerate(prod):
                if i != pca: prod[i] *= 0.5
            newcoord = np.array([prod[0], prod[1], point[2]])

            if tfed.any(): tfed = np.vstack([tfed, newcoord])
            else: tfed = np.array([newcoord])

            prod = np.dot(np.linalg.inv(vec), point[:2])
            #squish away the other axes
            for i, c in enumerate(prod):
                if i != pca: prod[i] *= 0.5
            newcoord = np.array([prod[0], prod[1], point[2]])

            if itfed.any(): itfed = np.vstack([itfed, newcoord])
            else: itfed = np.array([newcoord])


            prod = np.dot(vec, point[:2])
            for i, c in enumerate(prod):
                if i != pca: prod[i] *= 0.0
            prod = np.dot(np.linalg.inv(vec), prod)
            newcoord = np.array([prod[0], prod[1], point[2]])

            if iitfed.any(): iitfed = np.vstack([iitfed, newcoord])
            else: iitfed = np.array([newcoord])
            #print(np.dot(vertxy, vec).shape)
            #newv = np.reshape(point[:2], (2,1)) * vec 
            #print(newv.shape)
            #if not tfed.any(): tfed = np.array([point * vec])
            #else: tfed = np.vstack([tfed, point * vec])

        import matplotlib
        matplotlib.use('TkAgg')
        import matplotlib.pyplot as plt
        plt.plot(calphas[:,0], calphas[:,1], marker='.', linewidth=0)
        #plt.plot(tfed[:,0], tfed[:,1])
        plt.plot(iitfed[:,0], iitfed[:,1], marker='.', linewidth=0)

        for chain in centroids: 
            point = centroids[chain]
            plt.plot([point[0]], [point[1]], marker='+')
            plt.annotate(chain, point[:2], ha='center', va='center', color='lime', weight='black', size='large')
            plt.annotate(chain, point[:2], ha='center', va='center', color='black', weight='black')

        plt.xlim([-50,50])
        plt.ylim([-50,50])
        plt.title(self.fn)
        #plt.show()

