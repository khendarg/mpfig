#!/usr/bin/env python

import unittest

import opm
import opm.db

import os, shutil

#class TestOPM(unittest.TestCase):
#    def test_retrieve_one(self):
#        prefix = 'testopm/db_one'
#        if os.path.isdir(prefix): shutil.rmtree(prefix)
#        os.mkdir(prefix)
#
#        db = opm.db.OpmDatabase(prefix)
#        db.fetch(['2cfpA'])
#
#    def test_retrieve_multicomponent(self):
#        prefix = 'testopm/db_multicomp'
#        if os.path.isdir(prefix): shutil.rmtree(prefix)
#        os.mkdir(prefix)
#
#        db = opm.db.OpmDatabase(prefix)
#        db.fetch(['3jbr'])
#
#    def test_retrieve_several(self):
#        prefix = 'testopm/db_several'
#        if os.path.isdir(prefix): shutil.rmtree(prefix)
#        os.mkdir(prefix)
#
#        db = opm.db.OpmDatabase('testopm/db_several')
#        db.fetch(['4pgrA', '4pgsA', '4pguA'])
#
#    def test_retrieve_nonmembrane(self):
#        prefix = 'testopm/db_nonmembrane'
#        if os.path.isdir(prefix): shutil.rmtree(prefix)
#        os.mkdir(prefix)
#
#        db = opm.db.OpmDatabase('testopm/db_nonmembrane')
#        db.fetch(['4ins'])
#
#    def test_retrieve_nonpdb(self):
#        prefix = 'testopm/db_nonpdb'
#        if os.path.isdir(prefix): shutil.rmtree(prefix)
#        os.mkdir(prefix)
#
#        db = opm.db.OpmDatabase('testopm/db_nonpdb')
#        try: db.fetch(['21ns'])
#        except ValueError: return 0

class TestOPMAll(unittest.TestCase):
    def test_retrieve_all(self):
        prefix = 'testopm/db_all'
        #if os.path.isdir(prefix): shutil.rmtree(prefix)
        #os.mkdir(prefix)

        db = opm.db.OpmDatabase('testopm/db_all')
        try: db.fetch('all')
        except ValueError: return 0

    def test_load_all(self):
        prefix = 'testopm/db_all'

        db = opm.db.OpmDatabase('testopm/db_all')
        #as of 2018-12-14
        assert len(db.load('all')) == 14178

    def test_save_assignments(self):
        prefix = 'testopm/db_all'

        db = opm.db.OpmDatabase('testopm/db_all')
        assert len(db.load('all')) == 14178
        db.save_assignments('all')

if __name__ == '__main__':

    #if os.path.isdir('testopm'): shutil.rmtree('testopm')
    #os.mkdir('testopm')

    unittest.main()
