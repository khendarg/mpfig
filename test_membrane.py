#!/usr/bin/env python

from __future__ import print_function, division, unicode_literals

import unittest

import opm
import opm.db
import opm.membrane

import os, shutil

class TestMembrane(unittest.TestCase):
    def test_load(self):
        prefix = 'testopm/db_all'
        pdblist = ['3cx5', '2cfp']
        db = opm.db.OpmDatabase(prefix=prefix)

        indices = db.load(pdblist)
        #for k in sorted(indices): print(k, indices[k])


    def test_normals(self):
        #fn = 'testopm/db_all/pdb/3jbr.pdb'

        fn = 'testopm/db_all/pdb/3cx5.pdb'
        pdb = opm.membrane.Pdb(fn)
        pdb.get_normal()

        fn = 'testopm/db_all/pdb/2cfp.pdb'
        pdb = opm.membrane.Pdb(fn)
        pdb.get_normal()

    def nottest_fit_by_pca(self):
        prefix = 'testopm/db_all'
        #pdblist = ['3cx5', '2cfp', '1a0s', '3jbr']
        pdblist = ['6eyu']
        db = opm.db.OpmDatabase(prefix=prefix)

        indices = db.load(pdblist, splitchains=True)

        for pdbid in pdblist:
            pdb = opm.membrane.Pdb('{}/pdb/{}.pdb'.format(prefix, pdbid))
            pdb.fit_by_pca(indices[pdbid])

    def test_simplify_tms(self):
        prefix = 'testopm/db_all'
        #pdblist = ['3cx5', '2cfp', '1a0s', '3jbr']
        #pdblist = ['6eyu']
        pdblist = ['2cfp']
        db = opm.db.OpmDatabase(prefix=prefix)

        indices = db.load(pdblist, splitchains=True)

        for pdbid in pdblist:
            pdb = opm.membrane.Pdb('{}/pdb/{}.pdb'.format(prefix, pdbid))
            pdb.simplify_tms(indices[pdbid], subhels=2)


        

if __name__ == '__main__':

    #if os.path.isdir('testopm'): shutil.rmtree('testopm')
    #os.mkdir('testopm')

    unittest.main()
